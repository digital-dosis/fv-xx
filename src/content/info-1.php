<header class="info__header">
	<div class="info__panel">
		<img src="img/icons/ico-slide-02.svg" alt="Icono Preservar Agua Font Vella"/>
		<h1 class="title">Trabajando juntos por la preservación de los espacios naturales</h1>
		<p class="subtitle">Convenios para preservar los territorios y planes de acción para minimizar el impacto ambiental. En Font Vella trabajamos para preservar nuestro espacio natural protegido.</p>
	</div>
	<img class="panel-image" src="img/info/img-sabermas-04.jpg" alt="">
</header>
<section class="info__content">
	<p class="last">Nuestra agua de pura naturaleza nace en los manantiales situados en Sant Hilari Sacalm  y Sigüenza. Para ello, desde Font Vella nos comprometemos en preservar la naturaleza del entorno más próximo a nuestros manantiales conjuntamente con los ayuntamientos de Sant Hilari Sacalm y Sigüenza y las Asociaciones Selvans y Micorriza.</p>
	<img src="img/info/img-02-01.jpg" alt="Convenio Selvans con Font Vella y Ayuntamiento Sant Hilari" class="info__image info__image--medium">
	<h3>Convenio Selvans con Font Vella y el Ayuntamiento de Sant Hilari para la preservación del Espacio Natural de Les Guilleries (Sant Hilari Sacalm, Girona)</h3>
	<p class="last">En el espacio natural de Les Guilleries se crea nuestra agua de pura naturaleza. Se trata de un extraordinario ecosistema que debemos preservar. Para asegurar su conservación, hemos unido fuerzas con la <a href="">Asociación Sèlvans</a> y el Ayuntamiento de Sant Hilari Sacalm a través de la firma de un convenio de colaboración. Un proyecto en el que las tres entidades <strong>nos comprometemos a mantener la biodiversidad del territorio y a preservar su ciclo natural.</strong> Este acuerdo, firmado en 2019, incluye la puesta en marcha de diferentes medidas para la conservación de la flora y la fauna.</p>
	<h3 class="outer-title">Convenio Micorriza para la preservación del Parque Natural del Barranco Río Dulce (Sigüenza, Guadalajara)</h3>
	<p>El Parque Natural del Barranco Río Dulce es un lugar de alto valor medioambiental. Allí, en la ribera del arroyo del Vado, se crea nuestra agua de pura naturaleza. En colaboración con el Ayuntamiento de Sigüenza y la <a href="">Asociación Micorriza</a>, hemos firmado un convenio de colaboración para preservar y cuidar este espacio natural. Con ello <strong>nos comprometemos a elaborar un plan de acción para mantener la naturaleza autóctona del lugar</strong>, contribuyendo de esta manera, a preservar su ciclo natural.</p>
	<img src="img/info/img-02-02.jpg" alt="Convenio Micorriza" class="info__image info__image--medium">
	<h3>Jornadas de sensibilización medioambiental</h3>
	<p class="last">Juntos, trabajadores y ayuntamientos de las áreas donde se encuentran nuestros manantiales de agua de pura naturaleza, organizamos jornadas de sensibilización medioambiental para la protección del medio, el fomento del reciclaje, la concienciación de la importancia de la biodiversidad y la lucha contra el abandono de residuos.</p>

	<div class="info__btn">
		<div class="inner-btn"><a href="">En 2020 vamos más allá</a></div>
	</div>
</section>
