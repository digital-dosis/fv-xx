<header class="info__header">
	<div class="info__panel">
		<img src="img/icons/ico-slide-04.svg" alt="Icono Gota de Agua Font Vella"/>
		<h1 class="title">Recuperemos los entornos fluviales de seis ríos españoles.</h1>
		<p class="subtitle">En colaboración con la Fundación Biodiversidad hemos identificado dos áreas que necesitan especial protección en sus bosques de ribera. Estas son las zonas fluviales del río Segura y la comunidad autónoma de Cantabria.</p>
	</div>
	<img class="panel-image" src="img/info/img-sabermas-03.jpg" alt="">
</header>
<section class="info__content">
	<img src="img/info/logo-anse.jpg" alt="Logotipo ANSE" class="logo-anse">
	<h2 class="outer-title">Proyecto NATURAQUA2000 de la Asociación de Naturalistas del Sureste (ANSE)</h2>
	<p>Este proyecto tiene como objetivo conservar y restaurar el ecosistema de la cuenca del Segura. Este río, que recorre las provincias de Albacete, Murcia y Alicante, fue en el pasado uno de los más contaminados de Europa. Los esfuerzos e inversiones en materia de depuración han hecho posible que la calidad de sus aguas mejore notablemente. Proyectos como este, aseguran la continuidad de este trabajo y la recuperación de los valores ambientales.</p>
	<div class="info__list">
		<h4>Este proyecto se llevará a cabo:</h4>
		<p>Apoyando y cuidando a las especies autóctonas para conseguir su recuperación.</p>
		<p>Limpiando los residuos de la ribera para recuperar la calidad del agua.</p>
		<p>Aumentando la superficie de terrenos custodiados para asegurar que la preservación del río perdura en el tiempo.</p>
		<p>Involucrando a la ciudadanía para que participe en el proyecto y sienta que la naturaleza es un bien común que necesita ser cuidado.</p>
	</div>
	<p class="last">Fundada en 1973 para conservar, estudiar y recuperar la naturaleza en el sureste de la Península Ibérica, la Asociación de Naturalistas del Sureste (ANSE) vela por la conservación de la biodiversidad, el ecosistema y el paisaje. Si quieres consultar más información sobre este proyecto o ver qué otras actividades realiza ANSE, puedes visitar su <a href="">página web</a>.</p>

	<img src="img/info/logo-red-cambera.jpg" alt="Logotipo Red Cambera" class="logo-red-cambera">
	<h2>Proyecto Ríos de Red Cambera</h2>
	<p>Los ríos son una de las principales fuentes naturales de agua dulce. Es por ello que las cuencas fluviales deben ser protegidas. Este proyecto tiene como objetivo mejorar y aumentar la biodiversidad de cinco Zonas de Especial Conservación (ZEC)  fluviales de la Comunidad Autónoma de Cantabria, correspondientes a los ríos Deva, Saja, Pas, Miera y Asón. Se trata de un proyecto que se centra en la <strong>conservación de los bosques de ribera</strong>, esenciales para la vida del ecosistema.</p>
	<div class="info__list">
		<h4>Este proyecto se llevará a cabo:</h4>
		<p>Creando una Red de Custodia Fluvial que asegure la preservación del ecosistema.</p>
		<p>Preservando la fauna y flora de la red fluvial para mejorar la calidad del agua.</p>
		<p>Apoyando y cuidando a las especies autóctonas para conseguir su recuperación.</p>
		<p>Involucrando a la ciudadanía para que participe en el proyecto y sienta que la naturaleza es un bien común que necesita ser cuidado.</p>
	</div>
	<p class="last">Red Cambera es una asociación sin ánimo de lucro que trabaja desde el año 2010 para la conservación de la naturaleza. Proyectos como el suyo fomentan la conservación de estos ecosistemas fluviales. Si quieres consultar más detalles sobre este proyecto u otras actividades que realiza Red Cambera, puedes visitar su <a href="">página web.</a></p>

	<p class="text-center">Con la colaboración de:</p>
	<div class="info__logos">
		<a href="https://fundacion-biodiversidad.es/es"><img src="img/info/logo-fundacion.jpg" alt="Logotipo Ministerio para la transición ecológica" class="logo-ministerio"></a>
	</div>

	<div class="info__btn">
		<div class="inner-btn"><a href="">Actuamos por los espacios naturales</a></div>
	</div>
</section>
