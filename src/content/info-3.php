<header class="info__header">
	<div class="info__panel">
		<img src="img/icons/ico-slide-03.svg" alt="Icono Gota de Agua Font Vella"/>
		<h1 class="title">Únete a nuestro movimiento para preservar la biodiversidad de nuestros espacios naturales.</h1>
		<p class="subtitle">Los beneficios de las ventas del 21 de marzo irán destinados a financiar los proyectos de protección fluvial de la Asociación de Naturalistas del Sureste (ANSE) y Red Cambera.</p>
	</div>
	<img class="panel-image" src="img/info/img-sabermas-02.jpg" alt="">
</header>
<section class="info__content">
	<p>El 21 de marzo es el Día de los Bosques. Una fecha especial en la que <strong>queremos ir más allá de la protección de nuestros manantiales</strong> para cuidar también nuestros ríos. Las venas de la naturaleza por las que corre el agua dulce.</p>
	<p>Por eso, el próximo 21 de marzo destinamos todos nuestros beneficios a proyectos de las asociaciones ANSE y Red Cambera que tienen como objetivo la preservación de los bosques de ribera.</p>
	<p>Y es que, los bosques de ribera son fundamentales en el ecosistema fluvial. No solo retienen nutrientes y sedimentos, sino que también regulan la temperatura y la calidad del agua, controlan las crecidas de los ríos y son el refugio de la fauna.</p>
	<p class="last">Porque la mejor manera de proteger la pureza del agua es preservando los espacios naturales donde se origina.</p>

	<div class="info__btn">
		<div class="inner-btn"><a href="">Un agua de pura naturaleza</a></div>
	</div>
</section>
