<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Font Vella</title>
  <link rel="stylesheet" href="css/main.min.css">
  <link rel="stylesheet" href="css/vendors.min.css">
  <script src="js/vendors.min.js" charset="utf-8"></script>
</head>
<body>
	<div class="page story">
		<div class="loader js-loader">
		  <div class="loader__box js-loader-box">
		  	<div class="loader__background"></div>
		    <div class="loader__progress js-progress"></div>
		    <span class="loader__logo">
		      <img src="img/icons/logo-preloader.png"/>
		    </span>
		  </div>
		</div>
		<div class="pop-up js-popup-close">
		    <div class="pop-up__panel">
		        <div class="pop-up__close js-popup-close"></div>
		        <div class="pop-up__logo"><img src="img/icons/logo-espacios-naturales.png"/></div>
		        <div class="pop-up__text">
		        	<p>En el contexto en el que nos encontramos (COVID-19), nuestra prioridad es seguir asegurando el mejor nivel de servicio y de atención a nuestros clientes y consumidores en estos momentos.</p>
		        	<p>Por ello, hemos tomado la decisión de <strong>aplazar la iniciativa de Actuamos por los espacios naturales.</strong></p>
		        	<p>Nuestra dedicación de los beneficios de un día a proyectos de preservación de la biodiversidad y del agua, inicialmente prevista el 21 de Marzo, <strong>se retrasa al próximo 5 de junio, Día Mundial del Medioambiente</strong> (siempre que el contexto lo permita).</p>
		        	<p>Atentamente,<br>Equipo Font Vella</p>
		        </div>
		    </div>
		</div>
		<header class="header header--story">
			<div class="header__logo"><a href="/"><h1>Font Vella</h1></a></div>
			<div class="header__lang-selector js-story-nav">
				<div class="lang-selector">
					<span class="lang-selector__arrow js-lang-nav"></span>
					<a class="lang-selector__option lang-selector__option--is-active" href="">ESP</a>
					<a class="lang-selector__option" href="">CA</a>
				</div>
			</div>
			<div class="header__close js-close-info js-info-nav is-empty"></div>
		</header>
		<div class="story-container story-container">
			<div class="story-wrapper">
				<div class="story-slide chapter" data-hash="agua-dulce" data-chapter="0">
					<!-- Slide content chapter.php -->
					<div class="chapter__content">
						<img src="img/icons/logo-blue.svg" class="logo-blue" alt="Logotipo Font Vella">
						<h1 class="title chapter__title chapter__title-separator-top">
							<?php echo '<strong>¿Sabías que solo el 1% del agua es dulce y accesible?' . '</strong>' . ' <span>En Font Vella, actuamos por los espacios </span>naturales para preservar el agua.' ?>
						</h1>
						<div class="chapter__separator-icon chapter__separator-icon--water"><img src="img/icons/ico-slide-01.svg"/></div>
					</div>
					<!-- End -->
					<footer class="story__footer js-moreinfo js-story-nav" data-info="">
						<div class="story__more-info">
							<a><?php echo 'Saber más' ?></a>
						</div>
					</footer>
				</div>
				<div class="story-slide chapter" data-hash="espacios-naturales" data-chapter="1">
					<div class="chapter__content">
						<h1 class="title chapter__title chapter__title-separator-top">
							<?php echo 'Preservamos los <strong>espacios naturales ' . '</strong> ' . ' <span>donde se crea Font Vella</span>' ?>
						</h1>
						<div class="chapter__separator-icon chapter__separator-icon--origin"><img src="img/icons/ico-slide-02.svg"/></div>
						<div class="chapter__logos">
							<img src="img/icons/logo-ajuntament-santhilari.svg" alt="Logotipo Ayuntamiento Sant Hilari">
							<img src="img/icons/logo-siguenza.svg" alt="Logotipo Sigüenza">
							<img src="img/icons/logo-selvans.svg" alt="Logotipo Selvans">
							<img src="img/icons/logo-micorriza.svg" alt="Logotipo Micorriza">
						</div>
					</div>
					<footer class="story__footer js-moreinfo js-story-nav" data-info="">
						<div class="story__more-info">
							<a><?php echo 'Saber más' ?></a>
						</div>
					</footer>
				</div>
				<div class="story-slide chapter" data-hash="rios" data-chapter="2">
					<div class="chapter__content">
						<h1 class="title chapter__title chapter__title-separator-top">
							<?php echo 'Preservamos los <strong>espacios naturales ' . '</strong> ' . ' <span>de diferentes ríos en España</span>' ?>
						</h1>
						<div class="chapter__separator-icon chapter__separator-icon--support"><img src="img/icons/ico-slide-04.svg"/></div>
						<div class="chapter__logos">
							<img src="img/icons/logo-anse.svg" alt="Logotipo ANSE">
							<img src="img/icons/logo-red-cambera.svg" alt="Logotipo Red Cambera">
							<img class="logo-bio" src="img/icons/logo-fundacion-bio.png" alt="Logotipo Fundación Bio">
						</div>
					</div>
					<footer class="story__footer js-moreinfo js-story-nav" data-info="">
						<div class="story__more-info">
							<a><?php echo 'Saber más' ?></a>
						</div>
					</footer>
				</div>
				<div class="story-slide chapter" data-hash="21-marzo" data-chapter="3">
					<div class="chapter__content">
						<img src="img/icons/logo-actuamos.png" class="logo-actuamos" alt="Logotipo Font Vella Actuamos por los Espacios Naturales">
						<h1 class="title chapter__title">
							<?php echo 'Destinamos todos los <span><strong>beneficios de las ventas</strong></span> <span>del 21 de Marzo a proyectos</span> para preservar el agua.' ?>
						</h1>
						<div class="chapter__separator-icon chapter__separator-icon--act"><img src="img/icons/ico-slide-03.svg"/></div>
					</div>
					<footer class="story__footer js-moreinfo js-story-nav" data-info="">
						<div class="story__more-info">
							<a><?php echo 'Saber más' ?></a>
						</div>
					</footer>
				</div>
				<div class="story-slide chapter" data-hash="pura-naturaleza" data-chapter="4">
					<div class="chapter__content">
						<h1 class="title chapter__title chapter__title-separator-top">
							<?php echo 'Cuídate con Font Vella, un <strong>agua de pura naturaleza</strong>, ' . ' creada en espacios naturales protegidos.' ?>
						</h1>
						<div class="chapter__separator-icon chapter__separator-icon--protect"><img src="img/icons/ico-slide-05.svg"/></div>
					</div>
				</div>
			</div>
			<div class="story-pagination">
				<div class="story-pagination__chapter is-completed" data-chapter="0"></div>
				<div class="story-pagination__chapter" data-chapter="1"></div>
				<div class="story-pagination__chapter" data-chapter="2"></div>
				<div class="story-pagination__chapter" data-chapter="3"></div>
				<div class="story-pagination__chapter" data-chapter="4"></div>
			</div>
			<div class="story-button story-button--prev story-button-prev"></div>
    		<div class="story-button story-button--next story-button-next"></div>
		</div> <!-- End story -->
		<div class="info is-empty">
			<div class="info__outer"></div>
			<footer class="info__footer">
				<p>© Aguas Danone S.A.</p>
				<nav class="footer-nav">
					<a href="">Aviso legal</a>
					<a href="#" class="js-cookies-popup">Política de Cookies</a>
				</nav>
				<p>Web Design: <a href="http://digitaldosis.com" target="_blank">Digital Dosis</a></p>
			</footer>
		</div>
	</div> <!-- End page-wrapper -->
<!-- <script src="js/picturefill.min.js" charset="utf-8"></script> -->
<script src="js/main.js" charset="utf-8"></script>
</body>
</html>