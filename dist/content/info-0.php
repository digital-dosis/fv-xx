<header class="info__header">
	<div class="info__panel">
		<img src="img/icons/ico-slide-01.svg" alt="Icono Gota de Agua Font Vella"/>
		<h1 class="title title--sm">Nuestro recurso natural más valioso.</h1>
		<p class="subtitle">El agua dulce es vital para mantener la vida del planeta. Preservar sus espacios naturales de las amenazas actuales, es nuestro compromiso.</p>
	</div>
	<img class="panel-image" src="img/info/img-sabermas-01.jpg" alt="">
</header>
<section class="info__content">
	<p>El agua dulce es nuestro recurso natural más importante. La vida de seres humanos, animales y especies vegetales dependen de su existencia. <strong>Aunque tres cuartas partes del planeta están cubiertas por ella, tan solo el 1% de su caudal es dulce y accesible</strong>. Por eso, en Font Vella estamos comprometidos con su protección.</p>
	<img src="img/info/grafico-1porciento-es-mobile.png" alt="Gráfico agua dulce accesible en el planeta" class="world-graphic is-mobile">
	<img src="img/info/grafico-1porciento-es.png" alt="Gráfico agua dulce accesible en el planeta" class="world-graphic is-desktop">
	<p class="last">Los nuevos retos a los que se enfrenta nuestro planeta, como el cambio climático y el abastecimiento de los grandes núcleos urbanos, están haciendo que la calidad del agua dulce de ríos, lagos y corrientes subterráneas se resienta.</p>

	<h2>El agua dulce es nuestro recurso natural más importante.</h2>

	<p>Por eso, <strong>en Font Vella nos sentimos responsables del cuidado de las aguas subterráneas</strong> y de la preservación del origen de nuestra agua de pura naturaleza, creada en los espacios protegidos de Les Guilleries (Sant Hilari Sacalm, Girona) y el Parque Natural del Barranco Río Dulce (Sigüenza, Guadalajara).</p>

	<p>Sin embargo, sentimos que debemos ir todavía más allá de la protección de las aguas subterráneas de nuestros manantiales. Por eso, <strong>este año 2020 queremos impulsar proyectos de protección en seis de los ríos más importantes de nuestro país</strong>. Y es que estos son una de nuestras principales fuentes de agua dulce. Son las venas de la naturaleza. Los que dan vida a nuestro planeta.</p>

	<p>Para ello,  hemos creado el movimiento Actuamos por los Espacios Naturales para proteger la biodiversidad del agua.</p>

	<p class="last"><strong>Porque todos dependemos de este 1% y este 1% depende de nosotros</strong></p>

	<div class="info__btn">
		<div class="inner-btn"><a href="">Protegemos nuestros manantiales</a></div>
	</div>
</section>
