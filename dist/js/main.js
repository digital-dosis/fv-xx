
var handleEvents = function() {
	// Info
	$('.js-close-info').on('click', function(){
		$('.info, .js-info-nav').addClass('is-empty');
		$('.chapter__content, .js-story-nav').removeClass('is-empty');
		$('.info__outer').html('');
		scrolledOnce = false;
	});
	$('.js-moreinfo').on('click', function(){
		openInfo();
	});
	$('body').bind('DOMMouseScroll mousewheel', function(event) {
		if (event.originalEvent.wheelDelta < 0 || event.originalEvent.detail > 0) {
			slide_down();
		}
	});
	if(!mediaQuery('desktop')){
		$('body').bind('touchstart', function (e){
		   touchstart = e.originalEvent.touches[0].clientY;
		});
		$('body').bind('touchend', function (e){
		   var touchend = e.originalEvent.changedTouches[0].clientY;
		   if(touchstart > touchend+50){ // TO DO: ajustar rango sensible scroll down mobile
		      slide_down();
		   }
		});
		$(document).on('touchmove', function() {
		  $('body').trigger('mousewheel');
		});
	}
	$('.page').on('scroll', function(e) {
	    var scroll = $(this).scrollTop();
	    if(scroll > 100){
	        $('.header').addClass('header--is-hidden');
	    }else{
	     	$('.header').removeClass('header--is-hidden');
	    }
	});
	// Lang selector
	$('.js-lang-nav').on('click', function(){
		if(!$('.lang-selector').hasClass('is-open')){
			$('.lang-selector__option').slideDown();
			$('.lang-selector').addClass('is-open');
		}else{
			$('.lang-selector__option:not(.lang-selector__option--is-active)').slideUp();
			$('.lang-selector').removeClass('is-open');
		}
	});
	// Cookies
	$('.js-cookies-popup').on('click', function(e) {
		e.preventDefault();
		window.loadPopup();
	});
	//Pop-up
	$('.js-popup-close').on('click', function() {
		closePopup();
	});
}

var preloader = function(){
	var tmpImg = new Image();
	tmpImg.src = '';
    tmpImg.src = $('.loader__logo img').attr('src') ;
    tmpImg.onload = function() {
       $('.loader').addClass('is-loading');
       setTimeout(function () { 
			$('.loader').addClass('is-loaded');
		}, 800);
    };
}

var getURI = function(){
	var currentURL = window.location.href;
	if(currentURL.indexOf("?utm") > -1){
		//Remove Segment UTM if is presents
		currentURL = currentURL.slice(0, currentURL.indexOf("?utm"));
	}
	var lastURLSegment = currentURL.substr(currentURL.lastIndexOf('/') + 1);
	var pages = ['agua-dulce', 'espacios-naturales', 'rios', '21-marzo', 'pura-naturaleza'];
	var initPosition = pages.indexOf(lastURLSegment) >= 0 ? pages.indexOf(lastURLSegment) : 0;
	storySlider(initPosition);
	stepsPaginationStory(initPosition);
	//hashNavigate(initPosition);
}

var hashNavigate = function(activeIndex) {
	var hash = $('[data-chapter="' + activeIndex + '"]').data('hash');
	window.history.pushState({url: hash}, '', hash);
}

var storySlider = function(initPosition){
	var story = new Swiper('.story-container', {
		containerModifierClass: 'story-container-',
		wrapperClass: 'story-wrapper',
		slideClass: 'story-slide',
		slideActiveClass: 'story-slide-active',
		slideNextClass: 'story-slide-next',
		slidePrevClass: 'story-slide-prev',
	    speed: 400,
	    loop: true,
	    initialSlide: initPosition,
	    watchSlidesProgress: true,
	    navigation: {
	    	nextEl: '.story-button-next',
    		prevEl: '.story-button-prev',
	    },
	    a11y: {
	    	//Accessibility Parameters
	    	//TO DO:
	    	//Messages for screen readers when actions
	    	prevSlideMessage: 'Anterior slide',
    		nextSlideMessage: 'Siguiente slide',
	    	notificationClass: 'story-notification'
	    },
		on: {
			slideChange: function(){
				stepsPaginationStory(this.realIndex);
				hashNavigate(this.realIndex);
			}
		}
	});
}

//Steps Pagination Stories
var stepsPaginationStory = function( activeSlide ){
	$('.story-pagination__chapter').each(function( index ) {
		var $step = $(this);
		var chapterNumber = $step.data('chapter');
		if( chapterNumber <= activeSlide ){
			$step.addClass('is-completed');
		}else{
			$step.removeClass('is-completed');
		}
	});
}

//TO DO: se está llamando muchas veces a hasInfo debería llamarlo una unica vez
var hasInfo = function(){
	var $chapter = getActiveChapter();
	if($($chapter).find('.js-moreinfo').length !== 0){
		console.log('Cuantes veces entra?');
		scrolledOnce = true;
		return true;
	}else{
		console.log('No hooooombre');
		scrolledOnce = false;
		return false;
	}
}

var loadInfo = function(chapter){
	var url = "content/info-" + chapter + ".php"
	$( '.info__outer' ).load( url, function() {
		//changeContent
		$('.chapter__content, .js-story-nav').addClass('is-empty');
		TweenMax.set(".info__panel img, .info__panel h1, .info__panel p, .panel-image", {y:20,opacity:0});
		$('.info, .js-info-nav').removeClass('is-empty');
		setTimeout(function() {
		  TweenMax.staggerTo(".info__panel img, .info__panel h1, .info__panel p, .panel-image", 1, {y:0,opacity:1}, 0.2);
		}, 300);
	});
}

var openInfo = function(){
	loadInfo(getNumberActiveChapter());
}

var slide_down = function(){
	if(!scrolledOnce && !onPopup){
		if($('.info').hasClass('is-empty') && hasInfo()){
			openInfo();
		}
	}
}

var closePopup = function(){
	$('.pop-up').addClass('pop-up--is-hidden');
}

var getNumberActiveChapter = function(){
	var chapterNumber = $('.story-slide-active').data('chapter');
	return chapterNumber;
}

var getActiveChapter = function(){
	var chapter = $('.story-slide-active');
	return chapter;
}


$(document).ready(function(){
	console.log('Say Hey JS!!');
	preloader();
	getURI();
	handleEvents();
});